//#define REQUEST
//#define SEARCH
#define SET

unsigned char raub_AddressRequest[] = {0x0C,0x12,'D','M','X',' ','I','D',' ','R','E','Q','U','E','S','T',0,16};
unsigned int ruw_ID = 0;

unsigned char raub_Search[] = {0x02,0x15,'C','O','M','M','A','N','D',' ','S','E','A','R','C','H',' ','I','D',0,16};
unsigned char raub_Set[] = {0x04,0x1D,'C','O','M','M','A','N','D',' ','S','E','T',' ','D','M','X',' ','A','D','D','R','E','S','S',0,16};

void setup() {
  // put your setup code here, to run once:
  Serial.begin(250000,SERIAL_8N2);
  pinMode(1,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(1,HIGH);
  delayMicroseconds(17);
  digitalWrite(1,LOW);
  delayMicroseconds(100);

#ifdef REQUEST
  Serial.begin(250000,SERIAL_8N2);
  Serial.write(raub_AddressRequest,18);
  Serial.write((unsigned char)(ruw_ID >> 8u ));
  Serial.write((unsigned char)(ruw_ID >> 0u ));
  Serial.end();
  delay(2);

  if(ruw_ID < 0x0FFF)
  {
    ruw_ID++;
  }
  else
  {
    ruw_ID = 0;
  }
  #endif
  #ifdef SEARCH
  
  ruw_ID = 0x0150;
  Serial.begin(250000,SERIAL_8N2);
  Serial.write(raub_Search,21);
  Serial.write((unsigned char)(ruw_ID >> 8u ));
  Serial.write((unsigned char)(ruw_ID >> 0u ));
  Serial.end();
  delay(100);
  
  #endif
  #ifdef SET
  
  ruw_ID = 0x0150;
  Serial.begin(250000,SERIAL_8N2);
  Serial.write(raub_Set,27);
  Serial.write((unsigned char)(ruw_ID >> 8u ));
  Serial.write((unsigned char)(ruw_ID >> 0u ));
  Serial.write(0);
  Serial.write(1);
  Serial.end();
  delay(100);
  
  #endif

}
