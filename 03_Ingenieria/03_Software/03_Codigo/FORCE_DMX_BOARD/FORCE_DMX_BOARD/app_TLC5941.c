/*
* app_TLC5941.c
*
* Created: 28/07/2019 02:45:32 p. m.
*  Author: OmarSevilla
*/

#include "app_TLC5941.h"
#include "app_DMX.h"
#include "drv_GPIO.h"
#include "util/delay.h"

/* Private Definitions */
#define TLC5941_N_CHANNELS		16u
#define TLC5941_BITS_PER_CH		12u
#define TLC5941_MAX_CYCLES		255u

/* Private variables */
volatile static unsigned int lub_TempData;
static unsigned char rub_DataCounter;
static unsigned char rub_BitCounter;
static const unsigned char caub_DataIndex[TLC5941_N_CHANNELS] =
{
	12,
	13,
	14,
	15,
	8,
	9,
	10,
	11,
	4,
	5,
	6,
	7,
	3,
	2,
	1,
	0
};
static unsigned int ruw_GSCLKCounter;
static unsigned char rub_DotUpdateFlag;

/* Public Variables */
unsigned char rub_NewDataAvailable;

/************************************************************************/
/* Name: app_TLC5941_Init                                               */
/* Description: Initialize TLC5941 Update Application			    	*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_TLC5941_Init(void)
{
	rub_DotUpdateFlag = true;
	ruw_GSCLKCounter = 0u;
	rub_DataCounter = 0u;
	rub_BitCounter = 0u;
	DRV_GPIO_CLEAR_XLAT;
	DRV_GPIO_CLEAR_SCLK;
	DRV_GPIO_CLEAR_GSCLK;
	DRV_GPIO_CLEAR_SIN;
	DRV_GPIO_SET_MODE;
	DRV_GPIO_SET_BLANK;
	app_TLC5941_DotCorrection();
	DRV_GPIO_SET_XLAT;
	//_delay_us(10);
	DRV_GPIO_CLEAR_XLAT;
	DRV_GPIO_CLEAR_MODE;
}

/************************************************************************/
/* Name: app_TLC5941_Update                                             */
/* Description: Initialize TLC5941 Update Application					*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_TLC5941_Update(void)
{
	if (rub_NewDataAvailable == true)
	{
		//Wait for GSCLK Finish
	}
	else
	{
		DRV_GPIO_CLEAR_MODE;
		//Check if all data was sent
		if(rub_DataCounter >= TLC5941_N_CHANNELS)
		{//All data sent
			rub_NewDataAvailable = true;
			rub_DataCounter = 0u;
			rub_BitCounter = 0u;
			DRV_GPIO_CLEAR_SCLK;
			//rub_DotUpdateFlag = true;
		}
		else
		{
			DRV_GPIO_CLEAR_SCLK;
			if (rub_BitCounter >= TLC5941_BITS_PER_CH)
			{
				DRV_GPIO_CLEAR_SIN;
				rub_BitCounter = 0u;
				rub_DataCounter++;
			}
			else
			{
				
				lub_TempData = 	(0xFFu - app_DMXGetData(caub_DataIndex[rub_DataCounter])); //Read CH Data
				//Put next bit in SIN in MSB format
				lub_TempData = (((unsigned int)(lub_TempData) >> ((TLC5941_BITS_PER_CH - 1u) - rub_BitCounter)) & 0x0001u);
				
				if (lub_TempData != true)
				{
					DRV_GPIO_CLEAR_SIN;
				}
				else
				{
					DRV_GPIO_SET_SIN;
				}
				//_delay_us(100);
				//Set SCLK (Send bit)
				DRV_GPIO_SET_SCLK;
				//_delay_us(50);
				rub_BitCounter++;
			}
		}
	}
}

/************************************************************************/
/* Name: app_TLC5941_GSCLK                                              */
/* Description: TLC5941 GSCLK Task										*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_TLC5941_GSCLK(void)
{
	if (ruw_GSCLKCounter >= TLC5941_MAX_CYCLES)
	{
		if (rub_NewDataAvailable == true)
		{
			rub_NewDataAvailable = false;
			DRV_GPIO_SET_XLAT;
			DRV_GPIO_CLEAR_XLAT;
		}
		
		//Reset and Update Cycle
		DRV_GPIO_SET_BLANK;
		DRV_GPIO_CLEAR_BLANK;
		ruw_GSCLKCounter = 0;
	}
	else
	{
	}

	DRV_GPIO_SET_GSCLK;
	DRV_GPIO_CLEAR_GSCLK;
	ruw_GSCLKCounter++;
}

/************************************************************************/
/* Name: app_TLC5941_Task                                               */
/* Description: TLC5941 Main Task										*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_TLC5941_Task(void)
{
	app_TLC5941_GSCLK();
	app_TLC5941_Update();
}

/************************************************************************/
/* Name: app_TLC5941_DotCorrection                                      */
/* Description: TBD             										*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_TLC5941_DotCorrection(void)
{
	//Check if all data was sent
	if(rub_DataCounter >= TLC5941_N_CHANNELS)
	{//All data sent
		rub_DataCounter = 0u;
		rub_BitCounter = 0u;
		DRV_GPIO_CLEAR_SCLK;
		DRV_GPIO_SET_XLAT;
		DRV_GPIO_CLEAR_XLAT;
		rub_DotUpdateFlag = false;
	}
	else
	{
		DRV_GPIO_CLEAR_SCLK;
		if (rub_BitCounter >= 6u)
		{
			DRV_GPIO_CLEAR_SIN;
			rub_BitCounter = 0u;
			rub_DataCounter++;
		}
		else
		{
			
			lub_TempData = 	0xFF;
			//Put next bit in SIN in MSB format
			lub_TempData = (unsigned char)(((unsigned int)(lub_TempData) >> ((6 - 1u) - rub_BitCounter)) & 0x0001u);
			
			if (lub_TempData != true)
			{
				DRV_GPIO_CLEAR_SIN;
			}
			else
			{
				DRV_GPIO_SET_SIN;
			}
			//_delay_us(10);
			//Set SCLK (Send bit)
			DRV_GPIO_SET_SCLK;
			rub_BitCounter++;
		}
	}
}