/*
 * drv_UART.c
 *
 * Created: 17/07/2019 11:09:19 p. m.
 *  Author: OmarSevilla
 */ 

#include "drv_UART.h"

/************************************************************************/
/* Name: drv_UART_Init                                                  */
/* Description: Initialize the registers for UART operation             */
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void drv_UART_Init(void)
{
	(void)USART_0_init();
}
