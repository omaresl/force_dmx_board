/*
* app_DMX.c
*
* Created: 27/07/2019 06:32:56 p. m.
*  Author: OmarSevilla
*/

#include "app_DMX.h"
#include "util/delay.h"
#include "drv_GPIO.h"
#include "drv_UART.h"
#include "avr/eeprom.h"
#include "adre2p.h"
#include "app_TLC5941.h"
#include "app_Addresser.h"

/* Private typedefs */
typedef enum
{
	DMX_IDLE_STATE,
	DMX_BREAK_STATE,
	DMX_START_CODE_STATE,
	DMX_RX_DATA_STATE,
	DMX_ADDRESS
}T_DMX_STATE;

typedef enum
{
	AUT_RED,
	AUT_GREEN,
	AUT_BLUE,
	AUT_YELLOW,
	AUT_CYAN,
	AUT_PURPLE,
	AUT_WHITE
}T_COLOR_STATE;

typedef enum
{
	AUT_UP,
	AUT_DWN
}T_AUT_DIR;

/* Private definitions */
#define WAIT_IDLE_TIME			500
#define WAIT_BREAK_TIME			1000
#define WAIT_START_CODE_TIME	1000

/* Private Variables */
static T_DMX_STATE re_DMXState;
static unsigned int ruw_CycleCounter;
static unsigned char raub_DMXRXData[DMX_N_CHANNELS];
static unsigned char rub_CHIndex;
static unsigned int ruw_AddrCounter;
static unsigned int ruw_Addr = 0x01; //Address programmed
static unsigned int EEMEM euw_Address = 0x01u;
static unsigned char rub_DMXSignalFlag;
static T_AUT_DIR re_AutDir;
static T_COLOR_STATE re_AutColorState;

extern volatile unsigned char rub_Tick;
/************************************************************************/
/* Name: app_DMX_Init                                                   */
/* Description: Initialize DMX Application								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_DMX_Init(void)
{
	re_AutDir = AUT_RED;
	rub_DMXSignalFlag = false;
	re_DMXState = DMX_IDLE_STATE;
	DRV_GPIO_CLEAR_DE;
	eeprom_busy_wait();
	ruw_Addr = eeprom_read_word(&euw_Address);
}

/************************************************************************/
/* Name: app_DMXRX_SM                                                   */
/* Description: RX Application for DMX							        */
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_DMXRX_SM(void)
{
	unsigned char lub_tempData;
	
	switch (re_DMXState)
	{
		default:
		case DMX_IDLE_STATE:
		case DMX_BREAK_STATE:
		{
			DRV_UART_ENABLE_RX;
			if ((!DRV_UART_IS_RX_READY) && (ruw_CycleCounter < WAIT_BREAK_TIME))
			{
				ruw_CycleCounter++;
			}
			else
			{
				if (ruw_CycleCounter < WAIT_BREAK_TIME)
				{
					if ( (UCSR0A & (1 << FE0)) == (1 << FE0) )
					{
						if (DRV_UART_GET_DATA == 0)
						{
							rub_DMXSignalFlag = true;
							re_AutDir = AUT_RED;
							re_DMXState = DMX_START_CODE_STATE;
						}
						else
						{//Break value fail
							DRV_UART_DISABLE;
							re_DMXState = DMX_IDLE_STATE;
						}
					}
					else
					{//Break Time fail
						DRV_UART_DISABLE;
						re_DMXState = DMX_IDLE_STATE;
					}
				}
				else
				{//Timeout error
					DRV_UART_DISABLE;
					re_DMXState = DMX_IDLE_STATE;
					rub_DMXSignalFlag = false;
				}
				ruw_CycleCounter = 0;
			}
		}break;
		
		case DMX_START_CODE_STATE:
		{
			if ((!DRV_UART_IS_RX_READY) && (ruw_CycleCounter < WAIT_START_CODE_TIME))
			{
				ruw_CycleCounter++;
			}
			else
			{
				if (ruw_CycleCounter < WAIT_START_CODE_TIME)
				{
					lub_tempData = DRV_UART_GET_DATA;
					if (lub_tempData == 0)
					{
						ruw_AddrCounter = 0u;
						rub_CHIndex = 0u;
						re_DMXState = DMX_RX_DATA_STATE;
					}
					else
					{//Start code value fail
						app_DMXClearChannels();
						re_DMXState = DMX_ADDRESS;
						app_Addresser_SM(lub_tempData);
					}
				}
				else
				{//Timeout error
					DRV_UART_DISABLE;
					re_DMXState = DMX_IDLE_STATE;
				}
				ruw_CycleCounter = 0;
			}
		}break;
		
		case DMX_RX_DATA_STATE:
		{
			if ((!DRV_UART_IS_RX_READY) && (ruw_CycleCounter < WAIT_START_CODE_TIME))
			{
				ruw_CycleCounter++;
			}
			else
			{
				if (ruw_CycleCounter < WAIT_START_CODE_TIME)
				{
					lub_tempData = DRV_UART_GET_DATA;
					ruw_AddrCounter++;
					if (ruw_AddrCounter >= ruw_Addr)
					{
						raub_DMXRXData[rub_CHIndex] = lub_tempData;
						rub_CHIndex++;
						
						if (rub_CHIndex >= DMX_N_CHANNELS)
						{
							re_DMXState = DMX_IDLE_STATE;
						}
						else
						{//CH to be received
							/* Do nothing */
						}
					}
					else
					{
						/* Wait address */
					}
				}
				else
				{//Timeout error
					DRV_UART_DISABLE;
					re_DMXState = DMX_IDLE_STATE;
				}
				ruw_CycleCounter = 0;
			}
		}break;
		case DMX_ADDRESS:
		{
			app_Addresser_SM(0x00);
		}break;
	}
}

/************************************************************************/
/* Name: app_DMXDataTask                                                */
/* Description: Task													*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_DMXDataTask(void)
{
	if(rub_DMXSignalFlag == true)
	{
		if (raub_DMXRXData[0] > 100)
		{
			DRV_GPIO_CLEAR_LED_RED;
		}
		else
		{
			DRV_GPIO_SET_LED_RED;
		}
		
		if (raub_DMXRXData[1] > 100)
		{
			DRV_GPIO_CLEAR_LED_GREEN;
		}
		else
		{
			DRV_GPIO_SET_LED_GREEN;
		}
		
		if (raub_DMXRXData[2] > 100)
		{
			DRV_GPIO_CLEAR_LED_BLUE;
		}
		else
		{
			DRV_GPIO_SET_LED_BLUE;
		}
	}
	else
	{
		/* Wait for tick */
		if (rub_Tick == true)
		{
			rub_Tick = false;

			switch(re_AutColorState)
			{
				default:
				case AUT_RED:
				{
					if (re_AutDir == AUT_UP)
					{
						//Check the limit of CH1
						if(raub_DMXRXData[0] < 0xFF)
						{
							raub_DMXRXData[0]++;
							raub_DMXRXData[4] = raub_DMXRXData[0];
							raub_DMXRXData[8] = raub_DMXRXData[0];
							raub_DMXRXData[12] = raub_DMXRXData[0];
						}
						else
						{
							re_AutDir = AUT_DWN;
						}
					}//re_AutDir == AUT_UP
					else
					{
						//Check the limit of CH1
						if(raub_DMXRXData[0] > 0x00)
						{
							raub_DMXRXData[0]--;
							raub_DMXRXData[4] = raub_DMXRXData[0];
							raub_DMXRXData[8] = raub_DMXRXData[0];
							raub_DMXRXData[12] = raub_DMXRXData[0];
						}
						else
						{
							re_AutDir = AUT_UP;
							re_AutColorState = AUT_GREEN;
						}
					}
				}break;//case AUT_RED
				case AUT_GREEN:
				{
					if (re_AutDir == AUT_UP)
					{
						//Check the limit of CH1
						if(raub_DMXRXData[1] < 0xFF)
						{
							raub_DMXRXData[1]++;
							raub_DMXRXData[5] = raub_DMXRXData[1];
							raub_DMXRXData[9] = raub_DMXRXData[1];
							raub_DMXRXData[13] = raub_DMXRXData[1];
						}
						else
						{
							re_AutDir = AUT_DWN;
						}
					}
					else
					{
						//Check the limit of CH1
						if(raub_DMXRXData[1] > 0x00)
						{
							raub_DMXRXData[1]--;
							raub_DMXRXData[5] = raub_DMXRXData[1];
							raub_DMXRXData[9] = raub_DMXRXData[1];
							raub_DMXRXData[13] = raub_DMXRXData[1];
						}
						else
						{
							re_AutDir = AUT_UP;
							re_AutColorState = AUT_BLUE;
						}
					}
				}break;
				case AUT_BLUE:
				{
					if (re_AutDir == AUT_UP)
					{
						//Check the limit of CH2
						if(raub_DMXRXData[2] < 0xFF)
						{
							raub_DMXRXData[2]++;
							raub_DMXRXData[6] = raub_DMXRXData[2];
							raub_DMXRXData[10] = raub_DMXRXData[2];
							raub_DMXRXData[14] = raub_DMXRXData[2];
						}
						else
						{
							re_AutDir = AUT_DWN;
						}
					}
					else
					{
						//Check the limit of CH1
						if(raub_DMXRXData[2] > 0x00)
						{
							raub_DMXRXData[2]--;
							raub_DMXRXData[6] = raub_DMXRXData[2];
							raub_DMXRXData[10] = raub_DMXRXData[2];
							raub_DMXRXData[14] = raub_DMXRXData[2];
						}
						else
						{
							re_AutDir = AUT_UP;
							re_AutColorState = AUT_RED;
						}
					}
				}break;
			}
		}
		else
		{//Wait for tick
			
		}
	}
}

/************************************************************************/
/* Name: app_DMXTXTest                                                  */
/* Description: TX Application test for DMX							    */
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_DMXTXTest(void)
{
	unsigned int rul_CH_IDX = 0;
	
	DRV_GPIO_SET_TX_DATA;
	DRV_UART_DISABLE;
	_delay_ms(1);
	DRV_GPIO_CLEAR_TX_DATA;
	_delay_us(92);
	DRV_GPIO_SET_TX_DATA;
	DRV_UART_ENABLE_TX;
	_delay_us(12);
	DRV_UART_WRITE_DATA(0x00); //Start Code
	while(false != DRV_UART_IS_TX_BUSY)
	{
		//Wait for TX Done
	}
	for(rul_CH_IDX = 1; rul_CH_IDX <= 512u; rul_CH_IDX++)
	{

		DRV_GPIO_CLEAR_LED_GREEN;
		DRV_UART_WRITE_DATA(rul_CH_IDX);
		while(false != DRV_UART_IS_TX_BUSY)
		{
			//Wait for TX Done
		}
	}
	DRV_GPIO_SET_LED_GREEN;
}

/************************************************************************/
/* Name: app_DMXGetData                                                 */
/* Description: Export data from private array						    */
/* Parameters: Data Index                                               */
/* Return: unsigned char Element value                                  */
/************************************************************************/
unsigned char app_DMXGetData(unsigned char lub_DataIndex)
{
	if (lub_DataIndex <= DMX_N_CHANNELS)
	{
		return raub_DMXRXData[lub_DataIndex];
	}
	else
	{
		return 0;
	}
}

/************************************************************************/
/* Name: app_DMXResetSM                                                 */
/* Description: Reset DMX SM to IDLE									*/
/* Parameters: none														*/
/* Return: none															*/
/************************************************************************/
void app_DMXResetSM(void)
{
	DRV_UART_DISABLE;
	re_DMXState = DMX_IDLE_STATE;
	ruw_CycleCounter = 0;
}

/************************************************************************/
/* Name: app_DMXGetAddress                                              */
/* Description: Return address stored in eeprom   						*/
/* Parameters: none														*/
/* Return: unsigned int 												*/
/************************************************************************/
unsigned int app_DMXGetAddress(void)
{
	eeprom_busy_wait();
	return eeprom_read_word(&euw_Address);
}

/************************************************************************/
/* Name: app_DMXClearChannels                                           */
/* Description: Clear all DMX Data   									*/
/* Parameters: none														*/
/* Return: unsigned int 												*/
/************************************************************************/
void app_DMXClearChannels(void)
{
	for (unsigned int luw_i = 0; luw_i < DMX_N_CHANNELS; luw_i++)
	{
		raub_DMXRXData[luw_i] = 0x00;
	}
}

/************************************************************************/
/* Name: app_DMXSetREDChannels                                          */
/* Description: Set RED DMX Data     									*/
/* Parameters: none														*/
/* Return:  none        												*/
/************************************************************************/
void app_DMXSetREDChannels(void)
{
	for (unsigned int luw_i = 0; luw_i < DMX_N_CHANNELS; luw_i += 4)
	{
		raub_DMXRXData[luw_i] = 0xFF;
	}
}

/************************************************************************/
/* Name: app_DMXSetBLUEChannels                                         */
/* Description: Set BLUE DMX Data     									*/
/* Parameters: none														*/
/* Return: none          												*/
/************************************************************************/
void app_DMXSetBLUEChannels(void)
{
	for (unsigned int luw_i = 2; luw_i < DMX_N_CHANNELS; luw_i += 4)
	{
		raub_DMXRXData[luw_i] = 0xFF;
	}
}

/************************************************************************/
/* Name: app_DMXSetNewAddress                                           */
/* Description: Set BLUE DMX Data     									*/
/* Parameters: none														*/
/* Return: unsigned int 												*/
/************************************************************************/
void app_DMXSetNewAddress(unsigned int luw_Address)
{
	ruw_Addr = luw_Address;
	eeprom_write_word(&euw_Address,luw_Address);
}