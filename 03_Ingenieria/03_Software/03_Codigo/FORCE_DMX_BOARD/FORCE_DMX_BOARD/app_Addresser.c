/*
* app_Addresser.c
*
* Created: 07/08/2019 12:53:30 a. m.
*  Author: OmarSevilla
*/

#include "app_Addresser.h"
#include "app_ID.h"
#include "drv_UART.h"
#include "app_DMX.h"
#include "drv_GPIO.h"
#include "util/delay.h"
#include "app_TLC5941.h"

/* Private Definitions */
#define ADDRESS_REQUEST_PAYLOAD_SIZE_IDX	1
#define ADDRESS_REQUEST_CH_MODEL_IDX		17
#define ADDRESS_REQUEST_ADDRESS_HIGH_IDX	18
#define ADDRESS_REQUEST_ADDRESS_LOW_IDX		19

#define CMD_SEARCH_ID			0x02
#define CMD_SET_ADDRESS			0x04
#define CMD_ADDRESS_REQUEST		0x0C
#define CMD_ADDRESS_RESPONSE	0x0D

#define MAX_PAYLOAD	31
#define ADDRESSER_TIMEOUT	1000

/* Private Type Definitions */
typedef enum
{
	ADDRESS_REQUEST,
	ADDRESS_RESPONSE,
	ADDRESS_SEARCH_ID,
	ADDRESS_SET
}T_ADDRESSER_STATE;

/* Private Variables */
static volatile unsigned int ruw_UniqueID;
static T_ADDRESSER_STATE re_AddresserState;
static unsigned int ruw_AddresserCycleCounter;
static unsigned char raub_PayloadBuffer[MAX_PAYLOAD];
static unsigned char rub_PayloadIndex;

/* Private Function */
static void app_Addresser_SendResponse(void);

extern unsigned char rub_Tick;

/************************************************************************/
/* Name: app_Addresser_Init                                             */
/* Description: TBD                       								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_Addresser_Init(void)
{
	ruw_UniqueID = app_ID_GetID();
}

/************************************************************************/
/* Name: app_Addresser_SM                                               */
/* Description: TBD                       								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
void app_Addresser_SM(unsigned char lub_cmd)
{
	unsigned char lub_tempData;
	unsigned int luw_tempID;
	
	if (lub_cmd != 0u)
	{
		rub_PayloadIndex = 0;
		raub_PayloadBuffer[rub_PayloadIndex] = lub_cmd;
		rub_PayloadIndex++;
		ruw_AddresserCycleCounter = 0;
		if (lub_cmd == CMD_ADDRESS_REQUEST)
		{
			re_AddresserState = ADDRESS_REQUEST;
		}
		else if(lub_cmd == CMD_SEARCH_ID)
		{
			re_AddresserState = ADDRESS_SEARCH_ID;
		}
		else if(lub_cmd == CMD_SET_ADDRESS)
		{
			re_AddresserState = ADDRESS_SET;
		}
		else
		{
			//Reset DMX State Machine
			app_DMXResetSM();
		}
	}
	else
	{
		switch (re_AddresserState)
		{
			default:
			case ADDRESS_REQUEST:
			{
				if ((!DRV_UART_IS_RX_READY) && (ruw_AddresserCycleCounter < ADDRESSER_TIMEOUT))
				{
					ruw_AddresserCycleCounter++;
				}
				else
				{
					if (ruw_AddresserCycleCounter < ADDRESSER_TIMEOUT)
					{
						lub_tempData = DRV_UART_GET_DATA;
						raub_PayloadBuffer[rub_PayloadIndex] = lub_tempData;
						rub_PayloadIndex++;
						
						if(rub_PayloadIndex < (raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX] + 2u))
						{
							//Continue receiving
						}
						else
						{
							//Full Payload Received
							rub_PayloadIndex = 0u;
							re_AddresserState = ADDRESS_RESPONSE;
						}
					}
					else
					{//Timeout Error
						re_AddresserState = ADDRESS_REQUEST;
						//Reset DMX State Machine
						app_DMXResetSM();
					}
				}
			}break;
			case ADDRESS_RESPONSE:
			{
				if (raub_PayloadBuffer[ADDRESS_REQUEST_CH_MODEL_IDX] == DMX_N_CHANNELS)
				{
					luw_tempID = (unsigned int)raub_PayloadBuffer[ADDRESS_REQUEST_ADDRESS_LOW_IDX];
					luw_tempID |= (unsigned int)raub_PayloadBuffer[ADDRESS_REQUEST_ADDRESS_HIGH_IDX] << 8u;
					if (luw_tempID == ruw_UniqueID)
					{
						//Send Response with ID and Address
						//Fill buffer to be send
						raub_PayloadBuffer[0] = CMD_ADDRESS_RESPONSE;
						raub_PayloadBuffer[1] = 0x13;//Size 19 Bytes
						
						raub_PayloadBuffer[2]  = 'D';//D
						raub_PayloadBuffer[3]  = 'M';//M
						raub_PayloadBuffer[4]  = 'X';//X
						raub_PayloadBuffer[5]  = ' ';//
						raub_PayloadBuffer[6]  = 'I';//I
						raub_PayloadBuffer[7]  = 'D';//D
						raub_PayloadBuffer[8]  = ' ';//
						raub_PayloadBuffer[9]  = 'R';//R
						raub_PayloadBuffer[10] = 'E';//E
						raub_PayloadBuffer[11] = 'S';//S
						raub_PayloadBuffer[12] = 'P';//P
						raub_PayloadBuffer[13] = 'O';//O
						raub_PayloadBuffer[14] = 'N';//N
						raub_PayloadBuffer[15] = 'S';//S
						raub_PayloadBuffer[16] = 'E';//E
						raub_PayloadBuffer[17] = (unsigned char)(ruw_UniqueID >> 8u);//High Byte from ID
						raub_PayloadBuffer[18] = (unsigned char)(ruw_UniqueID >> 0u);//Low Byte from ID
						raub_PayloadBuffer[19] = (unsigned char)(app_DMXGetAddress() >> 8u);//High Byte from Address
						raub_PayloadBuffer[20] = (unsigned char)(app_DMXGetAddress() >> 0u);//Low Byte from Address
						
						//Send all data
						app_Addresser_SendResponse();
						
						re_AddresserState = ADDRESS_REQUEST;
						//When Finish Reset DMX SM
						app_DMXResetSM();
					}
					else
					{//Request for another ID
						re_AddresserState = ADDRESS_REQUEST;
						//Reset DMX State Machine
						app_DMXResetSM();
					}
				}
				else
				{//Model Incorrect
					re_AddresserState = ADDRESS_REQUEST;
					//Reset DMX State Machine
					app_DMXResetSM();
				}
			}break;
			case ADDRESS_SEARCH_ID:
			{
				if ((!DRV_UART_IS_RX_READY) && (ruw_AddresserCycleCounter < ADDRESSER_TIMEOUT))
				{
					ruw_AddresserCycleCounter++;
				}
				else
				{
					if (ruw_AddresserCycleCounter < ADDRESSER_TIMEOUT)
					{
						lub_tempData = DRV_UART_GET_DATA;
						raub_PayloadBuffer[rub_PayloadIndex] = lub_tempData;
						rub_PayloadIndex++;
						
						if(rub_PayloadIndex < (raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX] + 2u))
						{
							//Continue receiving
						}
						else
						{
							//Get model
							lub_tempData = raub_PayloadBuffer[raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX] - 1u];
							if(lub_tempData == DMX_N_CHANNELS)
							{
								//Full Payload Received
								luw_tempID = (unsigned int)raub_PayloadBuffer[raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX]] << 8u;
								luw_tempID |= raub_PayloadBuffer[raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX] + 1u];
								if (luw_tempID == ruw_UniqueID)
								{
									//Off All Channels
									app_DMXClearChannels();
									//On Red Channels
									app_DMXSetREDChannels();
									//UPDATE TLC5941 data
									luw_tempID = 0;
									//app_TLC5941_Init();
									while (luw_tempID < 256)
									{
										luw_tempID++;
										app_TLC5941_Task();
									}
									//TODO: Wait 30ms
									_delay_ms(30);
									//Off All Channels
									app_DMXClearChannels();
									while (luw_tempID < 256)
									{
										luw_tempID++;
										app_TLC5941_Task();
									}

									re_AddresserState = ADDRESS_REQUEST;
									//When Finish Reset DMX SM
									app_DMXResetSM();
								}
								else
								{
									//ID Search not valid
									re_AddresserState = ADDRESS_REQUEST;
									//When Finish Reset DMX SM
									app_DMXResetSM();
								}
							}
							else
							{//CH Model Not Valid
								re_AddresserState = ADDRESS_REQUEST;
								//When Finish Reset DMX SM
								app_DMXResetSM();
							}
						}
					}
					else
					{//Timeout Error
						re_AddresserState = ADDRESS_REQUEST;
						//Reset DMX State Machine
						app_DMXResetSM();
					}
				}
			}break;
			case ADDRESS_SET:
			{
				if ((!DRV_UART_IS_RX_READY) && (ruw_AddresserCycleCounter < ADDRESSER_TIMEOUT))
				{
					ruw_AddresserCycleCounter++;
				}
				else
				{
					if (ruw_AddresserCycleCounter < ADDRESSER_TIMEOUT)
					{
						lub_tempData = DRV_UART_GET_DATA;
						raub_PayloadBuffer[rub_PayloadIndex] = lub_tempData;
						rub_PayloadIndex++;
						
						if(rub_PayloadIndex < (raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX] + 2u))
						{
							//Continue receiving
						}
						else
						{
							//Get model
							lub_tempData = raub_PayloadBuffer[raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX] - 3u];
							if(lub_tempData == DMX_N_CHANNELS)
							{
								//Full Payload Received
								luw_tempID = (unsigned int)raub_PayloadBuffer[raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX] - 2] << 8u;
								luw_tempID |= raub_PayloadBuffer[raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX] - 1u];
								if (luw_tempID == ruw_UniqueID)
								{
									luw_tempID = ((unsigned int)raub_PayloadBuffer[raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX]] << 8u) | (raub_PayloadBuffer[raub_PayloadBuffer[ADDRESS_REQUEST_PAYLOAD_SIZE_IDX] + 1u]);
									app_DMXSetNewAddress(luw_tempID);
									
									//Off All Channels
									app_DMXClearChannels();
									//On Red Channels
									app_DMXSetBLUEChannels();
									//UPDATE TLC5941 data
									while (luw_tempID < 256)
									{
										luw_tempID++;
										app_TLC5941_Task();
									}
									//TODO: Wait 30ms
									_delay_ms(30);
									//Off All Channels
									app_DMXClearChannels();

									re_AddresserState = ADDRESS_REQUEST;
									//When Finish Reset DMX SM
									app_DMXResetSM();
								}
								else
								{
									//ID Search not valid
									re_AddresserState = ADDRESS_REQUEST;
									//When Finish Reset DMX SM
									app_DMXResetSM();
								}
							}
							else
							{//CH Model Not Valid
								re_AddresserState = ADDRESS_REQUEST;
								//When Finish Reset DMX SM
								app_DMXResetSM();
							}
						}
					}
					else
					{//Timeout Error
						re_AddresserState = ADDRESS_REQUEST;
						//Reset DMX State Machine
						app_DMXResetSM();
					}
				}
			}break;
		}
	}
}

/************************************************************************/
/* Name: app_Addresser_SendResponse                                     */
/* Description: TBD                       								*/
/* Parameters: None                                                     */
/* Return: None                                                         */
/************************************************************************/
static void app_Addresser_SendResponse(void)
{
	DRV_GPIO_SET_DE; //TX MODE
	
	//Break
	DRV_GPIO_SET_TX_DATA;
	DRV_UART_DISABLE;
	_delay_us(2);
	DRV_GPIO_CLEAR_TX_DATA;
	_delay_us(110);
	DRV_GPIO_SET_TX_DATA;
	DRV_UART_ENABLE_TX;
	_delay_us(12);
	
	for (rub_PayloadIndex = 0; rub_PayloadIndex < (raub_PayloadBuffer[1] + 2u); rub_PayloadIndex++)
	{
		DRV_UART_WRITE_DATA(raub_PayloadBuffer[rub_PayloadIndex]);
		while(false != DRV_UART_IS_TX_BUSY)
		{
			//Wait for TX Done
		}
	}
	DRV_UART_DISABLE;
	_delay_us(80);
	DRV_GPIO_CLEAR_DE; //RX MODE
}