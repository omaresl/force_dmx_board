#include <atmel_start.h>
#include "app_DMX.h"
#include "app_TLC5941.h"
#include "app_Addresser.h"

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	app_Addresser_Init();
	app_DMX_Init();
	app_TLC5941_Init();

	Enable_global_interrupt();
	/* Replace with your application code */
	while (1) {
		app_DMXRX_SM();
		app_DMXDataTask();
		app_TLC5941_Task();
	}
}
