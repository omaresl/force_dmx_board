/*
 * app_TLC5941.h
 *
 * Created: 28/07/2019 02:45:52 p. m.
 *  Author: OmarSevilla
 */ 


#ifndef APP_TLC5941_H_
#define APP_TLC5941_H_



/* Public functions */
extern void app_TLC5941_Init(void);
extern void app_TLC5941_Update(void);
extern void app_TLC5941_Task(void);
extern void app_TLC5941_GSCLK(void);
extern void app_TLC5941_DotCorrection(void);

extern unsigned char rub_NewDataAvailable;

#endif /* APP_TLC5941_H_ */