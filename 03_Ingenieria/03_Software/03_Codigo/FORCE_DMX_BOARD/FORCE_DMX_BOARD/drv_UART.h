/*
 * drv_UART.h
 *
 * Created: 17/07/2019 11:09:48 p. m.
 *  Author: OmarSevilla
 */ 


#ifndef DRV_UART_H_
#define DRV_UART_H_

/*Interfaces */
#include "usart_basic.h"

/* Public Definitions */

#define DRV_UART_DISABLE			USART_0_disable()
#define DRV_UART_ENABLE_RX			USART_0_enable_rx()
#define DRV_UART_ENABLE_TX			USART_0_enable_tx()
#define DRV_UART_GET_DATA			USART_0_get_data()
#define DRV_UART_WRITE_DATA(DATA)	USART_0_write(DATA)

#define DRV_UART_IS_RX_READY		USART_0_is_rx_ready()
#define DRV_UART_IS_TX_BUSY			USART_0_is_tx_busy()

/* Public functions */
extern void drv_UART_Init(void);

#endif /* DRV_UART_H_ */