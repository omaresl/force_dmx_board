/*
 * drv_GPIO.h
 *
 * Created: 17/07/2019 11:33:14 p. m.
 *  Author: OmarSevilla
 */ 


#ifndef DRV_GPIO_H_
#define DRV_GPIO_H_

/* Interfaces */
#include "atmel_start_pins.h"

/* Public Enumeration */

/* Public Definitions */
#define DRV_GPIO_SET_RX_DATA		RXD_DATA_set_level(0x01)
#define DRV_GPIO_SET_TX_DATA		TXD_DATA_set_level(0x01)
#define DRV_GPIO_SET_DE				DE_set_level(0x01)
#define DRV_GPIO_SET_MODE			MODE_set_level(0x01)
#define DRV_GPIO_SET_XLAT			XLAT_set_level(0x01)
#define DRV_GPIO_SET_BLANK			BLANK_set_level(0x01)
#define DRV_GPIO_SET_SIN			SIN_set_level(0x01)
#define DRV_GPIO_SET_SCLK			SCLK_set_level(0x01)
#define DRV_GPIO_SET_GSCLK			GSCLK_set_level(0x01)
#define DRV_GPIO_SET_LED_RED		LED_RED_set_level(0x01)
#define DRV_GPIO_SET_LED_GREEN		LED_GREEN_set_level(0x01)
#define DRV_GPIO_SET_LED_BLUE		LED_BLUE_set_level(0x01)

#define DRV_GPIO_CLEAR_RX_DATA		RXD_DATA_set_level(0x00)
#define DRV_GPIO_CLEAR_TX_DATA		TXD_DATA_set_level(0x00)
#define DRV_GPIO_CLEAR_DE			DE_set_level(0x00)
#define DRV_GPIO_CLEAR_MODE			MODE_set_level(0x00)
#define DRV_GPIO_CLEAR_XLAT			XLAT_set_level(0x00)
#define DRV_GPIO_CLEAR_BLANK		BLANK_set_level(0x00)
#define DRV_GPIO_CLEAR_SIN			SIN_set_level(0x00)
#define DRV_GPIO_CLEAR_SCLK			SCLK_set_level(0x00)
#define DRV_GPIO_CLEAR_GSCLK		GSCLK_set_level(0x00)
#define DRV_GPIO_CLEAR_LED_RED		LED_RED_set_level(0x00)
#define DRV_GPIO_CLEAR_LED_GREEN	LED_GREEN_set_level(0x00)
#define DRV_GPIO_CLEAR_LED_BLUE		LED_BLUE_set_level(0x00)

#define DRV_GPIO_GET_RX_DATA		RXD_DATA_get_level()
#define DRV_GPIO_GET_GSCLK			GSCLK_get_level()

#endif /* DRV_GPIO_H_ */