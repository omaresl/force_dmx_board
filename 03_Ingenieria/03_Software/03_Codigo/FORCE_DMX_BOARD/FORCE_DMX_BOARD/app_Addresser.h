/*
 * app_Addresser.h
 *
 * Created: 07/08/2019 12:54:13 a. m.
 *  Author: OmarSevilla
 */ 


#ifndef APP_ADDRESSER_H_
#define APP_ADDRESSER_H_


/* Public Functions */
extern void app_Addresser_Init(void);
void app_Addresser_SM(unsigned char lub_cmd);

#endif /* APP_ADDRESSER_H_ */